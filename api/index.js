/**
 * API dependencies
 */
import express from "express"

/** Import Hotels controller */
import hotelsController from "../controllers/hotels"

const router = express.Router()

/** 
 * GET
 * /hotels
 * Return all the hotels
 */
router.get("/hotels", (req, res) => {
    if (req.query.name == "" && req.query.stars == "") {
        const data = hotelsController.getAll()
        if (data.length > 0) {
            res.status(200).json(data)
        } else {
            /** Return 500 error if data.json doesn't exists */
            res.status(500).json({ "message": "data not found"});
        }
    } else {
        const filters = req.query
        const data = hotelsController.getByFilters(filters)
        res.status(200).json(data)
    }
})

export default router
