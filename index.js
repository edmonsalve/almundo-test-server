/**
* Server dependencies
 */
import http from "http"
import express from "express"
import cors from "cors"
import bodyParser from "body-parser"

/** Import API Module */
import apiRoot from "./api"

/** Utilities */
import { error, log } from "console";

const app = express()
/** Default port for development is 3001 */
const port = process.env.PORT || 3001

/** API VERSION */
const apiVersion = process.env.API_VERSION || "v1"

app.use(cors())
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))

/** Load the API routes behind the /api/{version} */
app.use(`/api/${apiVersion}`, apiRoot);


/** Create and run the http server */
const server = http.createServer(app)

server.listen(port, () => log(`Server is running on port ${port}`))