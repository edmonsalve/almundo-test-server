# ALMUNDO Test Server

Para ejecutar el API Rest usted puede usar los comandos:

`npm install`

`npm run start:dev` El cual lanzará un entorno de desarrollo incluido nodemon para escuchar los cambios
`npm run start:prod` El cual lanzara un entorno de producción

Nota: Si se encuentra en windows y desea usar el comando para producción, este no funciona de la misma manera, primero deberá setear las variables de la siguiente manera:
`set PORT=9000`
`set API_VERSION=v1`

Version de Node.js = 8.6.0

Espero les guste :) !