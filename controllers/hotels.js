/**
 * Controllers dependencies
 */


 
/** Load the JSON Object with the hotels data */
const data = require("../data.json")

/** Return all the hotels */
module.exports.getAll = () => {
    return data;
}

/** Return all the hotels  */
module.exports.getByFilters = (criteria) => {
    // Get the specific filters (name and stars)
    const name = criteria.name.toLowerCase();
    const stars = criteria.stars == "" ? 0 : parseInt(criteria.stars)
    // Filter the data 
    const result = data.filter((hotel) => {
        if (name !== "" && stars !== 0) {
            if (hotel.name.toLowerCase().includes(name) && hotel.stars == stars) return hotel
        } else if (name !== "" && stars == 0) {
            if (hotel.name.toLowerCase().includes(name)) return hotel
        } else if (name == "" && stars !== 0) {
            if (hotel.stars === stars) return hotel
        }
    })

    return result
}